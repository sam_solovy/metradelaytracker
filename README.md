# MetraDelayTracker
## Project Summary
 This webcrawler program will read Metra delays and log them to a database.
 The delays are read from the webpage: https://metrarail.com/riding-metra/digital-late-slips.
 The database is written as a MySQL database. 

## Getting Started
### Setting Up A MySQL Database
   1. Create a MySQL server. If you need help creating a server check out using "MySQL Workbench".
   The tutorial can be found here: https://dev.mysql.com/doc/workbench/en/
   2. Create a json file with the server configs in it. All mysql connections are made from this that file.
   An example of that file is stored at config.json.
   (yeah I know not really secured, but on a closed network it shouldn't matter.)
   3. Run in a python console  
   Example:  
```python
        import metra_database_setup from MetraDatabaseSetup
        metra_database_setup("config.js")
```
### Running A Metra Delay Crawler
List of Metra Lines can be found here: https://metrarail.com/maps-schedules  
Use the abbreviation: For example for the *Milwaukee District North* line use **MD-N**  
```python
        import MetraDelayCrawler from MetraDelays
        import datetime
        
        date = datetime.datetime.now()
        MetraDelayCrawler("MD-N", date, "config.js")   
```  
When an instance of the MetraDelayCrawler is created it will look up Metra delays and log them to the database.

### Setting Up Scheduled Task To Retrieve Delays On Windows
Using the windows *[Task Scheduler](https://en.wikipedia.org/wiki/Windows_Task_Scheduler)*:
  1. Create Task...  
  2. Create a Trigger that will execute every 4 hours.
  This is just to make sure that no delays are missed as Metra does not guaranteed delays will be kept on their website.
  3. Create a Action that:  
    1. Starts a program  
    2. The *Program/script* is the location on python.exe (C:\..\python.exe)  
    3. Has the arguments ```"AutoRunCrawler.py" "<server_config.json location>"```  
    4. Starts in the location of the *metradelaytracker* folder