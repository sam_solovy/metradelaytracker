import json
import mysql.connector
from mysql.connector import errorcode


def metra_database_setup(server_config, database_name="metra"):
    """
        Sets up the mysql database needed for the capturing metra delays.
        Code is based off of mysql connector example at
        https://dev.mysql.com/doc/connector-python/en/connector-python-example-ddl.html
    :param server_config: the mysql server config json file
    :param database_name: the name that the database should be called, defaults to `metra`
    :return: nothing
    """

    trains = {
        "BNSF": "BNSF Railway",
        "HC": "Heritage Corridor",
        "ME": "Metra Electric",
        "MD-N": "Milwaukee North",
        "MD-W": "Milwaukee West",
        "NCS": "North Central Service",
        "RI": "Rock Island",
        "SWS": "Southwest Service",
        "UP-N": "Union Pacific North",
        "UP-NW": "Union Pacific Northwest",
        "UP-W": "Union Pacific West",
    }

    tables = {
        "train_lines": (
            "CREATE TABLE `train_lines` ("
            "   `Name` varchar(10) NOT NULL UNIQUE,"
            "   `Id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,"
            "   `Description` varchar(50) DEFAULT NULL,"
            "   PRIMARY KEY (`Id`)"
            ") ENGINE=InnoDB"),
        "train_stops": (
            "CREATE TABLE `train_stops` ("
            "   `Name` varchar(20) NOT NULL,"
            "   `Id` int(11) NOT NULL UNIQUE AUTO_INCREMENT,"
            "   `Line Id` int(11) NOT NULL,"
            "   PRIMARY KEY (`Id`)"
            ") ENGINE=InnoDB"),
        "delays": (
            "CREATE TABLE `delays` ("
            "   `Id` varchar(20) NOT NULL UNIQUE,"
            "   `Expected Time` TIME NOT NULL,"
            "   `Actual Time` TIME NOT NULL,"
            "   `Date` DATE NOT NULL,"
            "   `Line Id` int(11) NOT NULL,"
            "   `Stop Id` int(11) NOT NULL,"
            "   `Trip` int(11) NOT NULL,"
            "   PRIMARY KEY (`Id`),"
            "   CONSTRAINT `delay_line_id` FOREIGN KEY (`Line Id`)"
            "     REFERENCES `train_lines` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,"
            "   CONSTRAINT `delay_stop_id` FOREIGN KEY (`Stop Id`)"
            "     REFERENCES `train_stops` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION"
            ") ENGINE=InnoDB")
    }
    data = json.load(open(server_config, 'r'))
    cnx = mysql.connector.connect(user=data['database']['user'], password=data['database']['password'],
                                  host=data['database']['host'], port=data['database']['port'])
    # only need these config values
    cursor = cnx.cursor()

    def create_database(my_sql_cursor):
        try:
            my_sql_cursor.execute(
                "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(database_name))
        except mysql.connector.Error as error:
            print("Failed creating database: {}".format(error))
            exit(1)
        print("Creating DB {}: ".format(database_name), end='')
    try:
        cnx.database = database_name
        print(cnx.database)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_DB_ERROR:
            create_database(cursor)
            cnx.database = database_name
        else:
            print(err)
            exit(1)
    for name, ddl in tables.items():
        try:
            print("Creating table {}: ".format(name), end='')
            cursor.execute(ddl)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")
    for train in trains.keys():
        try:
            print("Adding train {}: ".format(train), end=' ')
            insert = "INSERT INTO `train_lines` (Name, Description)"\
                " VALUES ('{}','{}')".format(train, trains[train])
            print(insert)
            cursor.execute("INSERT INTO train_lines (Name, Description) VALUES (%s, %s);", (train, trains[train]))
            print("OK")
        except mysql.connector.Error as err:
                print(err.msg)
    cnx.commit()
    cursor.close()
    cnx.close()
