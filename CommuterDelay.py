class TrainDelay:
    def __init__(self, line, stop, trip_number, date, scheduled_time, actual_time):
        """
            Constructs the a train delay.
        :param line: The train line
        :param stop: The train stop
        :param trip_number: The train number
        :param date: The date for the delay
        :param scheduled_time: The time of when the train was scheduled to arrive at the station
        :param actual_time: The time of when the train got to the station
        """
        self._line = line
        self._stop = stop
        self._trip_number = trip_number
        self._date = date
        self._scheduled_time = scheduled_time
        self._actual_time = actual_time

    def line(self):
        """
            Returns the train line
        :return: the train line
        """
        return self._line

    def stop(self):
        """
            Returns the train stop
        :return: the train stop
        """
        return self._stop

    def trip_number(self):
        """
            Returns the trip number of the train
        :return: the trip number of the train
        """
        return self._trip_number

    def date(self):
        """
            Returns the date of the delay
        :return: the date of the delay
        """
        return self._date

    def scheduled_time(self):
        """
            Returns the time of when the train was scheduled to be at the train stop.
        :return: the time of when the train was scheduled to be at the train stop.
        """
        return self._scheduled_time

    def actual_time(self):
        """
            Returns the time when the train got to the station
        :return: the time when the train got the station
        """
        return self._actual_time
