import MetraDelays
import datetime
import sys


def create_auto_run_crawler(server_config):
    """
        Runs a web crawler to record all the metra delays around the current time.
    :param server_config:  the mysql server config json file
    :return: Nothing
    """
    lines = MetraDelays.get_train_lines(server_config)
    for line in list(lines.keys()):
        MetraDelays.MetraDelayCrawler(line, datetime.datetime.now(), server_config)


if __name__ == "__main__":
    create_auto_run_crawler(sys.argv[1])
