import requests
import datetime
from bs4 import BeautifulSoup
import mysql.connector
import sys
import json
from CommuterDelay import TrainDelay as Delay


class MetraDelayCrawler:
    """
        Logs metra delays to a database.
    """
    def __init__(self, line, date, server_config):
        """
            Initializes a MetraDelayCrawler which will log delays from a given time to a DB.
        :param line: The metra line to look for delays for
        :param date: The date time to look for delays. Datetime object
        :param server_config: The configuration file for the MySql DB
        """
        self._server_config = server_config
        self._form_id = self._get_form_id()
        self._delays = self._get_metra_delays_for_line(line, date, self._form_id)
        self._trains_lines = get_train_lines(self._server_config)
        self._log_delays_to_db()

    def form_id(self):
        return self._form_id

    def delays(self):
        return self._delays

    def print_delays(self):
        """
            Prints a readable print stream of the delays.
        :return:
        """
        for delay in self._delays:
            print("line: {}, stop: {}, scheduled time: {}, actual time {}, date {}"
                  .format(delay.line(), delay.stop(), delay.scheduled_time(), delay.actual_time(), delay.date()))

    def _get_form_id(self):
        """
            Because Metra makes this hard. To call the ajax call for the delays we need a form id.
            This value is actually on the email button. So we will read the page and retrieve it.

        :return: string of the form_build_id
        """
        r = requests.get("https://metrarail.com/riding-metra/digital-late-slips")
        data = r.text
        if r.text is None:
            raise ConnectionError("Did not retrieve html from https://metrarail.com/riding-metra/digital-late-slips")
        soup = BeautifulSoup(data, 'html.parser')
        inputs = soup.findAll("input")
        form_build_id_input = next(inputEle for inputEle in inputs if inputEle.get("name") == "form_build_id")
        return form_build_id_input.get("value")

    def _get_metra_delays_for_line(self, line, date_time, form_id):
        """
            Gets the delayed stops for a given line before the current time.
        :param line: the metra line to look for delays before the current time.
        :param date_time: the dateTime to look for delays.
        :param form_id: the form_id of the the metra website session.
        :return: array of delayed objects
        """
        if not isinstance(date_time, datetime.datetime):
            raise ValueError("{} should be a 'datetime' object".format(date_time))
        time = date_time.time().strftime("%I:%M %p")
        date_in = date_time.date().strftime("%Y-%m-%d")
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Requested-With": "XMLHttpRequest",
            "Referer": "https://metrarail.com/riding-metra/digital-late-slips"
        }
        payload = {
            "date[date]": date_in,
            "date[time]": time,
            "route": line,
            "form_id": "digital_late_slips_form",
            "form_build_id": form_id
        }
        r = requests.post("https://metrarail.com/system/ajax", headers=headers, data=payload)
        data = r.json()[2]['data']  #throw specific error if this fails.
        soup = BeautifulSoup(data, 'html.parser')
        delayed_list = []
        for option in soup.find_all("option"):
            delay = self._construct_delay(option)
            delayed_list.append(delay)
        return delayed_list

    @staticmethod
    def _construct_delay(html_option):
        """
            Creates a train delay object from a string of the train line, and html that contains the metadata
            for the delay.
        Example <option value=\"BNSF|2/13/2018 1:37:34 PM|NAPERVILLE|1231\">Trip 1231 at Naperville scheduled at 1:31 PM</option>
        :param htmlOption: The htmlOption tag that contains all the meta data
        :return: a train delay
        """
        message = html_option.string
        scheduled_time = message[message.find("at", message.find("scheduled")) + 3:]
        scheduled_time = datetime.datetime.strptime(scheduled_time, "%I:%M %p")
        scheduled_time = scheduled_time.time().strftime("%H:%M:%S")
        line, unformatted_date, stop, trip_number = html_option["value"].split("|")
        formatted_date = datetime.datetime.strptime(unformatted_date, "%m/%d/%Y %I:%M:%S %p")
        actual_time = formatted_date.time().strftime("%H:%M")
        date_out = formatted_date.date().strftime("%Y-%m-%d")
        return Delay(line, stop, trip_number, date_out, scheduled_time, actual_time)

    def _log_delays_to_db(self):
        """
            Logs the list of delays to the database
        :return: Nothing
        """
        try:
            data = json.load(open(self._server_config, 'r'))
            cnx = mysql.connector.connect(**data["database"])
            cursor = cnx.cursor(buffered=True)
            for delay in self._delays:
                try:
                    if type(delay) is Delay:
                        line_id = self._trains_lines.get(delay.line())
                        query_stop = "SELECT Id FROM train_stops WHERE Name='{}' AND `Line Id` = {}"\
                            .format(delay.stop(), line_id)
                        insert_stop = "INSERT INTO train_stops (Name, `Line Id`) Values ('{}', {})".format(delay.stop(), line_id)

                        cursor.execute(query_stop)
                        stop_id = cursor.fetchone()
                        if stop_id is None:
                            cursor.execute(insert_stop)
                            cursor.execute(query_stop)
                            stop_id = cursor.fetchone()
                        stop_id = stop_id[0]
                        # create unique Id
                        delay_id = delay.date().replace("-", "") \
                            + delay.scheduled_time().replace(":", "") + str(line_id) + str(stop_id)
                        insert_sql = ("INSERT INTO delays (`Id`, `Expected Time`, `Actual Time`, Date, `Stop Id`, `Line Id`, `Trip`)"
                                      "VALUES ( '{}', '{}', '{}', '{}', '{}', '{}' )")\
                            .format(int(delay_id), delay.scheduled_time(), delay.actual_time(),
                                        delay.date(), stop_id, line_id, delay.trip_number())
                        cursor.execute(insert_sql)
                except:
                    print(sys.exc_info()[0])
                    print(delay_id)
                    continue
            cnx.commit()
            cnx.close()
        except IOError:
            print("Cannot read JSON configfile, {}".format(self._server_config))


def get_train_lines(server_config):
        """
        Returns a key value map of all train lines registered in the database.
        :return: A dictionary of train lines where the Key is the Train Line name and the Value is the id of the line
        """
        lines = {}
        try:
            data = json.load(open(server_config, 'r'))
            cnx = mysql.connector.connect(**data["database"])
            cursor = cnx.cursor()
            query_sql = "SELECT Id, Name FROM train_lines"
            cursor.execute(query_sql)
            for (Id, Name) in cursor:
                lines[Name] = Id
            cursor.close()
            cnx.close()
        except IOError:
            print("Cannot read JSON configfile, '{}'".format(server_config))
        return lines
